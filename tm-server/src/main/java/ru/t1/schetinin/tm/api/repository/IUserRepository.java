package ru.t1.schetinin.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.model.User;

import java.util.List;

public interface IUserRepository {

    @Insert("INSERT INTO user_table (id, login, password_hash, email, locked, " +
            "first_name, last_name, middle_name, role)" +
            " VALUES (#{id}, #{login}, #{passwordHash}, #{email}, #{locked}, " +
            "#{firstName}, #{lastName}, #{middleName}, #{role})")
    void add(@NotNull User user);

    @Delete("DELETE FROM user_table")
    void clear();

    @Select("SELECT * FROM user_table")
    @Results(value = {@Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    @Nullable List<User> findAll();

    @Select("SELECT * FROM user_table WHERE id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    @Nullable User findOneById(@NotNull @Param("id") String id);

    @Select("SELECT * FROM user_table WHERE LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    @Nullable User findOneByIndex(@NotNull @Param("index") Integer index);

    @Select("SELECT COUNT(*) FROM user_table")
    int getSize();

    @Delete("DELETE FROM user_table WHERE id = #{id}")
    void remove(@NotNull User user);

    @Update("UPDATE user_table SET login = #{login}, password_hash = #{passwordHash}, email = #{email}, " +
            "locked = #{locked}, first_name = #{firstName}, last_name = #{lastName}, " +
            "middle_name = #{middleName}, role = #{role} WHERE id = #{id}")
    void update(@NotNull User user);

    @Select("SELECT * FROM user_table WHERE login = #{login} LIMIT 1")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    @Nullable User findByLogin(@NotNull @Param("login") String login);

    @Select("SELECT * FROM user_table WHERE email = #{email} LIMIT 1")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    @Nullable User findByEmail(@NotNull @Param("email") String email);

}
